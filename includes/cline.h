/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cline.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 17:56:30 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 14:59:18 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLINE_H
# define CLINE_H

# include <stdint.h>
# include <stdbool.h>
# include <stdlib.h>
# include <unistd.h>
# include <libft.h>

typedef struct			s_cch
{
	wchar_t			ch;
	struct s_cch	*next;
	struct s_cch	*prev;
}						t_cch;

t_cch					*new_cch(wchar_t ch);
void					link_cch(t_cch *left, t_cch *right);
void					del_cch(t_cch *chain);

typedef struct			s_cline {
	t_cch	*begin;
	t_cch	*end;
	t_cch	*cursor;
	int		len;
	int		pos;
}						t_cline;

t_cline					*new_cline(void);
t_cline					*copy_cline(t_cline *old, int len);
int						print_cline(t_cch *chain);
void					clear_cline(t_cline *cline);
void					free_cline(t_cline *cline);

t_cch					*push_ch(t_cline *cline, wchar_t ch);
void					put_ch(t_cline *cline, wchar_t ch);
void					del_cch_cline(t_cline *cline, t_cch *cch);

int						is_space_cline(t_cline *cline);
int						is_num_cline(t_cline *cline);
int						cline_cmp(t_cline *c1, t_cline *c2);

int						cline_to_int(t_cline *cline);
char					*cline_to_str(t_cline *cline);
t_cline					*str_to_cline(char *str);
t_cline					*split_cline(t_cline *cline, int pos);

#endif
