/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 13:54:23 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 20:14:08 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EXEC_H
# define EXEC_H

# include <limits.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <sys/stat.h>
# include <errno.h>
# include "cline.h"
# include "parse.h"
# include "libft.h"
# include "builtins.h"

char			**g_env;

char			**copy_env(char *old_env[]);
void			free_env(char *env[]);
char			*push_env_var(char **env[], char *var_str);
char			*set_env_var(char **env[], char *var, char *value);
void			del_env_var(char **env[], char *var);
char			*get_env_var(char *env[], char *var);
char			*get_var_value(char *var);
char			*replace_env_var(char *env[], char *var, char *value);
char			*replace_env_var_str(char *env[], char *var_str);
void			start_env(char **env[]);

int				get_argv(t_token **token_list, char **argv_ptr[]);
void			free_argv(int argc, char **argv);

void			del_meta_char(t_cline *cline);
bool			is_var_end(wchar_t c);
t_cch			*del_var(t_cline *cline, t_cch *cch);
char			*get_var_name(t_cch *cch);
void			insert_value(t_cline *cline, t_cch *cch, t_cline *value_cline);
void			subst_var(t_cline *cline, t_cch **cch);
void			subst_home_var(t_cline *cline, t_cch **cch);
void			subst_vars(t_cline *cline);

typedef struct	s_fd {
	int		stdin;
	int		stdout;
	int		stderr;
	int		pipe[2];
	int		hdoc[2];
	t_cline	*hdoc_cline;
}				t_fd;

# define FILE_RIGHTS (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
# define TRUNC_MODE (O_WRONLY | O_CREAT | O_TRUNC)
# define APPND_MODE (O_WRONLY | O_CREAT | O_APPEND)
# define READ_MODE (O_RDONLY)

int				run_cmd(t_cmd *cmd, t_fd *fd);
int				run_pipe(t_pipe *pipe);
int				run_pipe_list(t_pipe *pipe);

typedef enum	e_path {
	PATH_OK,
	PATH_NOEXIST,
	PATH_NOTDIR,
	PATH_ISDIR,
	PATH_NOPERM
}				t_path;

int				run_ext_cmd(int argc, char *argv[], char *env[], t_fd *fd);
t_path			find_cmd(char *cmd_name, const char *path_var, char *cmd_path);

# define STDFD(x) ((x >= STDIN_FILENO && x <= STDERR_FILENO))

t_rs			cline_handler(t_cline *cline);

int				redirect(t_fd *fd, t_rd *rd);
int				read_file_redirect(t_rd *rd, t_fd *fd);
int				write_file_redirect(t_rd *rd, t_fd *fd);
int				descriptor_redirect(t_rd *rd, t_fd *fd);
int				heredoc_redirect(t_rd *rd, t_fd *fd);

int				check_fd(t_cline *fd_cline);

void			get_hdocs(t_pipe *pipe_list, t_rs *rs);

#endif
