/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/18 18:05:25 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/21 19:02:05 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSE_H
# define PARSE_H

# include "cline.h"

typedef enum	e_meta {
	META_NOMETA,
	META_SPACE,
	META_COLON,
	META_PIPE,
	META_LESS,
	META_GREAT,
	META_AMSND
}				t_meta;

typedef enum	e_op {
	OP_ERR = -1,
	OP_NOOP,
	OP_END,
	OP_CMDEND,
	OP_PIPE,
	OP_REIN,
	OP_REIN_IO,
	OP_REOUT,
	OP_REOUT_IO,
	OP_REOUTAP,
	OP_REOUTAP_IO,
	OP_HDOC,
	OP_REOUTFD,
	OP_REOUTFD_IO,
	OP_REERRFD = OP_ERR
}				t_op;

typedef	struct	s_token_op {
	t_cch	*cur;
	int		len;
	char	is_qtd;
	t_op	op;
}				t_token_op;

t_meta			is_meta(wchar_t ch);
t_op			get_op(t_cline *cline);
t_op			get_token(t_cline **src_cline, t_cline **token_cline);

typedef struct	s_rd {
	t_cline		*from;
	t_cline		*to;
	t_op		op;
	struct s_rd	*next;
}				t_rd;

t_rd			*rd_new(void);
t_rd			*rd_push(t_rd **list, t_rd *rd);
void			free_rd(t_rd *list);

typedef struct	s_token {
	t_cline			*cline;
	struct s_token	*next;
}				t_token;

typedef struct	s_cmd {
	t_token			*token_list;
	t_rd			*rd_list;
	struct s_cmd	*next;
}				t_cmd;

typedef struct	s_pipe {
	t_cmd			*cmd_list;
	struct s_pipe	*next;
}				t_pipe;

t_token			*token_new(t_cline *token_cline);
void			token_free(t_token *token);

t_cmd			*cmd_new(void);
t_token			*cmd_push_token(t_cmd *cmd, t_token *token);
void			cmd_free(t_cmd *cmd);

t_pipe			*pipe_new(void);
t_cmd			*pipe_push_cmd(t_pipe *pipe, t_cmd *cmd);
void			pipe_free(t_pipe *pipe);

void			free_pipe_list(t_pipe *pipe_list);
t_pipe			*push_pipe_list(t_pipe **pipe_list_ptr, t_pipe *pipe);

void			create_ast(t_pipe **pipe_list_ptr, t_cline *token_cline,
						t_op op);
void			get_redir_num(t_token **token_list, t_rd *rd, t_op op);

#endif
