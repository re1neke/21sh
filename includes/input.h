/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 17:56:30 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 11:48:41 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INPUT_H
# define INPUT_H

# include <stdint.h>
# include <stdbool.h>
# include <stdlib.h>
# include <unistd.h>
# include <term.h>
# include <signal.h>
# include <libft.h>
# include "cline.h"

# define ESC_BUF_SIZE (uint8_t)7

typedef uint8_t			t_esc_seq[ESC_BUF_SIZE];
typedef enum			e_seq_num
{
	ESC_FAIL,
	ESC_UP,
	ESC_DOWN,
	ESC_RIGHT,
	ESC_LEFT,
	ESC_DEL,
	ESC_HOME,
	ESC_END,
	ESC_CUP,
	ESC_CDOWN,
	ESC_CRIGHT,
	ESC_CLEFT
}						t_seq_num;

# ifdef __linux__

static const t_esc_seq	g_esc_codes[] = {
	{91, 65, 0, 0, 0, 0, 0},
	{91, 66, 0, 0, 0, 0, 0},
	{91, 67, 0, 0, 0, 0, 0},
	{91, 68, 0, 0, 0, 0, 0},
	{91, 51, 126, 0, 0, 0, 0},
	{91, 72, 0, 0, 0, 0, 0},
	{91, 70, 0, 0, 0, 0, 0},
	{91, 49, 59, 53, 65, 0, 0},
	{91, 49, 59, 53, 66, 0, 0},
	{91, 49, 59, 53, 67, 0, 0},
	{91, 49, 59, 53, 68, 0, 0}
};

# elif __APPLE__

static const t_esc_seq	g_esc_codes[] = {
	{91, 65, 0, 0, 0, 0, 0},
	{91, 66, 0, 0, 0, 0, 0},
	{91, 67, 0, 0, 0, 0, 0},
	{91, 68, 0, 0, 0, 0, 0},
	{91, 51, 126, 0, 0, 0, 0},
	{91, 72, 0, 0, 0, 0, 0},
	{91, 70, 0, 0, 0, 0, 0},
	{91, 49, 59, 53, 65, 0, 0},
	{91, 49, 59, 53, 66, 0, 0},
	{91, 49, 59, 53, 67, 0, 0},
	{91, 49, 59, 53, 68, 0, 0}
};

# else
#  error "button's codes for this OS is not set"
# endif

static const t_seq_num	g_codes_count = sizeof(g_esc_codes) / sizeof(t_esc_seq);

typedef enum			e_esc_cmp {
	ESC_CMP_EQ,
	ESC_CMP_NEQ,
	ESC_CMP_CON
}						t_esc_cmp;

t_seq_num				get_esc_seq(void);

typedef struct			s_scr {
	int	pos;
	int	width;
}						t_scr;

t_scr					g_scr;

void					move_cursor(int to_move);

void					move_left(t_cline **cline_ptr);
void					move_right(t_cline **cline_ptr);
void					move_right_for(t_cline **cline_ptr, int offset);
void					move_left_for(t_cline **cline_ptr, int offset);

void					move_down(t_cline **cline_ptr);
void					move_up(t_cline **cline_ptr);

void					home(t_cline **cline_ptr);
void					end(t_cline **cline_ptr);

void					next_word(t_cline **cline_ptr);
void					prev_word(t_cline **cline_ptr);

void					backspace(t_cline *cline);
void					del(t_cline **cline_ptr);

void					next_cline(t_cline **cline_ptr);
void					prev_cline(t_cline **cline_ptr);

void					copy_input_cline(t_cline *cline);
void					paste_input_cline(t_cline *cline);

typedef	void			(*t_esc_hdlr)(t_cline **);

static const t_esc_hdlr	g_esc_hdlrs[] = {
	NULL,
	prev_cline,
	next_cline,
	move_right,
	move_left,
	del,
	home,
	end,
	move_up,
	move_down,
	next_word,
	prev_word
};

void					set_term(void);
void					unset_term(void);

void					set_sign(void);
void					unset_sign(void);

# define KEY_ESC 0033
# define KEY_EOT 0004
# define KEY_EOL 0012
# define KEY_DEL 0177

# define KEY_R 18
# define KEY_W 23

typedef struct			s_hist {
	t_cline			*cline;
	struct s_hist	*next;
	struct s_hist	*prev;
}						t_hist;

t_cline					*g_cur_cline;

typedef void			(*t_hist_free_hdlr)(t_cline *);

t_hist					*new_hist_node(t_cline *cline);
void					push_hist_node(t_hist *node);
void					free_hist(t_hist_free_hdlr cline_hdlr);
void					reset_hist_cursor(void);

void					next_hist_node(t_cline **cline_ptr);
void					prev_hist_node(t_cline **cline_ptr);

typedef enum			e_cause {
	NO_CAUSE = -1,
	OK,
	QUOTE,
	DQUOTE,
	PIPE,
	SLSH,
	HDOC
}						t_cause;

typedef struct			s_cheker {
	bool	quote;
	bool	dquote;
	bool	pipe;
	int		bslsh;
}						t_cheker;

t_cause					check_cline(t_cline *cline);
int						prompt(t_cause cause);

typedef enum			e_rs {
	READ_OK,
	READ_DROP,
	READ_EXIT
}						t_rs;

t_rs					read_cline(t_cline **cline);

typedef t_rs			(*t_cline_hdlr)(t_cline *cline);

void					shell_loop(t_cline_hdlr hdlr);

#endif
