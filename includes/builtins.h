/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:51:54 by mvladymy          #+#    #+#             */
/*   Updated: 2019/07/09 21:54:39 by reineke          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTINS_H
# define BUILTINS_H

# include <libft.h>
# include "input.h"
# include "exec.h"

bool				g_exit;

typedef int			(t_bltn_hdlr)(int argc, char **argv, char **env);

int					bltn_exit(int argc, char **argv, char **env);
int					bltn_clear(int argc, char **argv, char **env);
int					bltn_env(int argc, char **argv, char **env);
int					bltn_setenv(int argc, char **argv, char **env);
int					bltn_unsetenv(int argc, char **argv, char **env);
int					bltn_cd(int argc, char **argv, char **env);
int					bltn_pwd(int argc, char **argv, char **env);
int					bltn_echo(int argc, char **argv, char **env);
int					bltn_which(int argc, char **argv, char **env);
int					bltn_history(int argc, char **argv, char **env);

typedef struct		s_bltn {
	char		*name;
	t_bltn_hdlr	*hdlr;
}					t_bltn;

static const t_bltn	g_builtins[] = {
	{"clear", bltn_clear},
	{"exit", bltn_exit},
	{"env", bltn_env},
	{"setenv", bltn_setenv},
	{"unsetenv", bltn_unsetenv},
	{"cd", bltn_cd},
	{"pwd", bltn_pwd},
	{"echo", bltn_echo},
	{"which", bltn_which},
	{"history", bltn_history}
};

static const unsigned int
g_builtins_cnt = sizeof(g_builtins) / sizeof(t_bltn);

t_bltn_hdlr			*get_bltn(char *name);

char				*make_logic_path(const char *path);
int					is_logic(char **argv[], bool *logic, const char *cmd);
#endif
