/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_logic.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/08 13:44:02 by mvladymy          #+#    #+#             */
/*   Updated: 2019/07/08 13:44:02 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"

int	is_logic(char **argv[], bool *logic, const char *cmd)
{
	*logic = true;
	while (**argv)
	{
		if (!ft_strcmp(**argv, "-P"))
			*logic = false;
		else if (!ft_strcmp(**argv, "-L"))
			*logic = true;
		else if ((**argv)[0] == '-' && (**argv)[1] != '\0')
		{
			ft_dprintf(STDERR_FILENO,
				"21sh: %s: %s: nema takogo praporu 🙄\n", cmd, **argv);
			return (-1);
		}
		else
			break ;
		(*argv)++;
	}
	return (0);
}