/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_builtin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:58:11 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/24 18:04:41 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"

t_bltn_hdlr	*get_bltn(char *name)
{
	unsigned int	i;

	i = g_builtins_cnt;
	while (i--)
	{
		if (!ft_strcmp(g_builtins[i].name, name))
			return (g_builtins[i].hdlr);
	}
	return (NULL);
}
