/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 18:45:05 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/26 20:09:59 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"

int	bltn_env(int argc, char **argv, char **env)
{
	(void)argc;
	(void)argv;
	while (*env)
		ft_printf("%s\n", *env++);
	return (0);
}

int	bltn_setenv(int argc, char **argv, char **env)
{
	int	i;
	int	exit_status;

	(void)env;
	i = 1;
	exit_status = 0;
	while (i < argc)
	{
		if (ft_strchr(argv[i], '=') == argv[i])
		{
			ft_dprintf(STDERR_FILENO,
					"21sh: setenv: `%s' tak pysaty ne mozhna 😕\n", argv[i++]);
			exit_status = 1;
			continue ;
		}
		push_env_var(&g_env, argv[i++]);
	}
	return (exit_status);
}

int	bltn_unsetenv(int argc, char **argv, char **env)
{
	int	i;
	int	exit_status;

	(void)env;
	i = 1;
	exit_status = 0;
	while (i < argc)
	{
		if (ft_strchr(argv[i], '='))
		{
			ft_dprintf(STDERR_FILENO,
					"21sh: unsetenv: `%s' tak pysaty ne mozhna 😕\n", argv[i++]);
			exit_status = 1;
			continue ;
		}
		del_env_var(&g_env, argv[i++]);
	}
	return (exit_status);
}
