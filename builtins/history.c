/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 15:06:32 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/30 15:08:34 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"

extern t_hist	*g_hist;

int	bltn_history(int argc, char *argv[], char *env[])
{
	t_hist	*cur;
	int		i;

	(void)argc;
	(void)argv;
	(void)env;
	if (!(cur = g_hist))
		return (-1);
	while (cur->prev)
		cur = cur->prev;
	i = 1;
	while (cur)
	{
		ft_printf("%2d. ", i++);
		if (cur->cline)
			print_cline(cur->cline->begin);
		ft_putchar('\n');
		cur = cur->next;
	}
	return (0);
}
