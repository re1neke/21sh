/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   some_builtins.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:51:02 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/27 16:55:45 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"

int		bltn_exit(int argc, char **argv, char **env)
{
	(void)argc;
	(void)argv;
	(void)env;
	g_exit = true;
	return (0);
}

int		bltn_clear(int argc, char **argv, char **env)
{
	(void)argc;
	(void)argv;
	(void)env;
	ft_dprintf(STDIN_FILENO, tgetstr("cl", NULL));
	return (0);
}

int		bltn_which(int argc, char **argv, char **env)
{
	char	path[PATH_MAX];

	if (argc <= 1 || find_cmd(argv[1],
			get_var_value(get_env_var(env, "PATH")), path) != PATH_OK)
		return (1);
	ft_printf("%s\n", path);
	return (0);
}
