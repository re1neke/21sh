/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc_redirect.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 20:02:30 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 20:14:12 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

int	heredoc_redirect(t_rd *rd, t_fd *fd)
{
	if (!rd->from)
		return (-1);
	close(fd->hdoc[0]);
	close(fd->hdoc[1]);
	if (pipe(fd->hdoc) < 0)
		return (-1);
	fd->stdin = fd->hdoc[0];
	fd->hdoc_cline = rd->to;
	return (0);
}
