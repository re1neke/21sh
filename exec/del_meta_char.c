/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   del_meta_char.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:24:27 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/25 14:29:30 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

void	del_meta_char(t_cline *cline)
{
	t_cch	*cch;
	t_cch	*tmp;
	char	is_qtd;

	is_qtd = '\0';
	cch = cline->begin;
	while (cch)
	{
		is_qtd -= (is_qtd == '\'' + 1) ? 1 : 0;
		tmp = cch;
		if ((cch->ch == '\'' || cch->ch == '"') &&
				(is_qtd == cch->ch || is_qtd == '\0'))
		{
			is_qtd = (is_qtd) ? '\0' : (char)cch->ch;
			is_qtd += (is_qtd == '\'') ? 1 : 0;
		}
		cch = (cch->next && is_qtd != '\'' && cch->ch == '\\') ?
				cch->next->next : cch->next;
		if (is_qtd != '\'' &&
				(tmp->ch == '\'' || tmp->ch == '"' || tmp->ch == '\\'))
			del_cch_cline(cline, tmp);
	}
}
