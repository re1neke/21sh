/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_cmd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:49:11 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/28 20:45:19 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static int	run_bltn(t_bltn_hdlr *bltn, t_fd *fd, int argc, char *argv[])
{
	int	exit_status;
	int	old_fd[3];

	old_fd[0] = dup(STDIN_FILENO);
	old_fd[1] = dup(STDOUT_FILENO);
	old_fd[2] = dup(STDERR_FILENO);
	dup2(fd->stdin, STDIN_FILENO);
	dup2(fd->stdout, STDOUT_FILENO);
	dup2(fd->stderr, STDERR_FILENO);
	if (fd->stdin != STDIN_FILENO)
		close(fd->stdin);
	if (fd->stdout != STDOUT_FILENO)
		close(fd->stdout);
	if (fd->stderr != STDERR_FILENO)
		close(fd->stderr);
	exit_status = bltn(argc, argv, g_env);
	dup2(old_fd[0], STDIN_FILENO);
	dup2(old_fd[1], STDOUT_FILENO);
	dup2(old_fd[2], STDERR_FILENO);
	close(old_fd[0]);
	close(old_fd[1]);
	close(old_fd[2]);
	return (exit_status);
}

int			run_cmd(t_cmd *cmd, t_fd *fd)
{
	char		**argv;
	int			argc;
	int			exit_status;
	t_bltn_hdlr	*bltn;

	if (!~(argc = get_argv(&cmd->token_list, &argv)))
		return (-1);
	if (!argc)
		exit_status = 0;
	else if ((bltn = get_bltn(argv[0])))
		exit_status = run_bltn(bltn, fd, argc, argv);
	else
		exit_status = run_ext_cmd(argc, argv, g_env, fd);
	free_argv(argc, argv);
	return (exit_status);
}
