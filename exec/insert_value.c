/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert_value.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 18:28:25 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/24 18:33:36 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

void	empty_line(t_cline *cline, t_cline *value_cline)
{
	cline->begin = value_cline->begin;
	cline->end = value_cline->end;
}

void	end_line(t_cline *cline, t_cline *value_cline)
{
	cline->end->next = value_cline->begin;
	value_cline->begin->prev = cline->end;
	cline->end = value_cline->end;
}

void	begin_line(t_cline *cline, t_cline *value_cline)
{
	cline->begin->prev = value_cline->end;
	value_cline->end->next = cline->begin;
	cline->begin = value_cline->begin;
}

void	in_line(t_cch *cch, t_cline *value_cline)
{
	cch->prev->next = value_cline->begin;
	value_cline->begin->prev = cch->prev;
	cch->prev = value_cline->end;
	value_cline->end->next = cch;
}

void	insert_value(t_cline *cline, t_cch *cch, t_cline *value_cline)
{
	if (!cline->begin)
		empty_line(cline, value_cline);
	else if (!cch)
		end_line(cline, value_cline);
	else if (cch == cline->begin)
		begin_line(cline, value_cline);
	else
		in_line(cch, value_cline);
	cline->len += value_cline->len;
	free(value_cline);
}
