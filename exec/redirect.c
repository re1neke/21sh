/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirect.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 16:22:59 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 20:05:53 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

int	redirect(t_fd *fd, t_rd *rd)
{
	int	exit_status;

	exit_status = 0;
	while (rd)
	{
		if (rd->op == OP_REOUT || rd->op == OP_REOUTAP
				|| rd->op == OP_REOUT_IO || rd->op == OP_REOUTAP_IO)
			exit_status = write_file_redirect(rd, fd);
		else if (rd->op == OP_REIN || rd->op == OP_REIN_IO)
			exit_status = read_file_redirect(rd, fd);
		else if (rd->op == OP_REOUTFD || rd->op == OP_REOUTFD_IO)
			exit_status = descriptor_redirect(rd, fd);
		else if (rd->op == OP_HDOC)
			exit_status = heredoc_redirect(rd, fd);
		rd = rd->next;
	}
	return (exit_status);
}
