/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 19:32:38 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 19:53:29 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static t_cline	*cat_cline(t_cline **c1, t_cline *c2)
{
	if (!*c1)
	{
		*c1 = c2;
		return (*c1);
	}
	else if (!c2)
		return (*c1);
	push_ch(*c1, '\n');
	if ((*c1)->end)
		(*c1)->end->next = c2->begin;
	if (c2->begin)
		c2->begin->prev = (*c1)->end;
	if (c2->end)
		(*c1)->end = c2->end;
	(*c1)->cursor = NULL;
	(*c1)->len += c2->len;
	(*c1)->pos = (*c1)->len;
	free(c2);
	return (*c1);
}

static t_rs		read_hdoc(t_rd *rd)
{
	t_cline	*tmp_cline;
	t_rs	rs;

	while (true)
	{
		prompt(HDOC);
		rs = read_cline(&tmp_cline);
		ft_putchar_fd('\n', STDIN_FILENO);
		if (rs == READ_EXIT || cline_cmp(tmp_cline, rd->from))
		{
			free_cline(tmp_cline);
			break ;
		}
		else if (rs == READ_DROP)
		{
			free_cline(tmp_cline);
			return (READ_DROP);
		}
		cat_cline(&rd->to, tmp_cline);
	}
	if (rd->to)
		push_ch(rd->to, '\n');
	return (READ_OK);
}

static t_rs		find_hdoc(t_rd *rd)
{
	t_rs	rs;

	rs = READ_OK;
	while (rd)
	{
		if (rd->op == OP_HDOC && rd->from)
		{
			if ((rs = read_hdoc(rd)) != READ_OK)
				break ;
			if (rd->to)
				subst_vars(rd->to);
		}
		rd = rd->next;
	}
	return (rs);
}

void			get_hdocs(t_pipe *pipe_list, t_rs *rs)
{
	t_cmd	*cmd;

	if (*rs != READ_OK)
		return ;
	while (pipe_list)
	{
		cmd = pipe_list->cmd_list;
		while (cmd)
		{
			find_hdoc(cmd->rd_list);
			cmd = cmd->next;
		}
		pipe_list = pipe_list->next;
	}
}
