# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/18 17:53:56 by mvladymy          #+#    #+#              #
#    Updated: 2019/05/28 16:34:51 by mvladymy         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re $(NAME) norm
.NOTPARALLEL: re norm

NAME=21sh
SRCPATH=./
SRC=main.c
BINPATH=bin/
BIN=$(SRC:.c=.o)
INCPATH=includes/
INC=builtins.h cline.h exec.h input.h parse.h
LIBS=libft/libft.a input/libinput.a cline/libcline.a parser/libparser.a \
	builtins/libbuiltins.a exec/libexec.a

export LFTINCPATH=../libft/includes

CC=clang
CFLAG=-Wall -Wextra -Werror -Wconversion
INCFLAG=-I$(INCPATH) -Ilibft/includes -Iinput

EXEC="✴️ "
FAIL="❌ "
OK="✅"


all: $(NAME)

$(NAME): $(LIBS) $(addprefix $(BINPATH), $(BIN))
	@echo $(EXEC) "Linking" $@ "executable" && \
	$(CC) $(CFLAG) $(INCFLAG) $(addprefix $(BINPATH), $(BIN)) -o $@ \
	$(addprefix -L, $(dir $(LIBS))) -lexec -lbuiltins -linput -lparser \
	-lcline -lft -ltermcap && \
	echo $(OK) "Done" || \
	echo $(FAIL) "Failed"

$(addprefix $(BINPATH), %.o): $(addprefix $(SRCPATH), %.c) \
	$(addprefix $(INCPATH), $(INC))
	@mkdir -p $(BINPATH)
	@$(CC) $(CFLAG) $(INCFLAG) -c $< -o $@

%.a: force_look
	@echo $(EXEC) "Compiling" $@ && \
	$(MAKE) -C $(dir $@) -j4 && \
	echo $(OK) "Done" || \
	echo $(FAIL) "Failed"

force_look:
	@true

clean:
	@echo $(EXEC) "Deleting object files"
	@rm -rf $(BINPATH)
	@for libdir in $(dir $(LIBS)); do \
		$(MAKE) -C $$libdir clean; \
	done
	@echo $(OK) "Done"

fclean: clean
	@echo $(EXEC) "Deleting binary files"
	@rm -f $(NAME)
	@rm -rf $(BINPATH)
	@for libdir in $(dir $(LIBS)); do \
		$(MAKE) -C $$libdir fclean; \
	done
	@echo $(OK) "Done"

re: fclean all

norm:
	@echo $(EXEC) "Norming source files" && \
	norminette $(addprefix $(SRCPATH), $(SRC)) $(addprefix $(INCPATH), \
	$(INC)) && \
	for libdir in $(dir $(LIBS)); do \
		$(MAKE) -C $$libdir norm; \
	done && \
	echo $(OK) "Done" || \
	echo $(FAIL) "Failed"
