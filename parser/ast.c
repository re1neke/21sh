/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 12:49:59 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/21 18:59:29 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse.h"

static void	parse_redirection(t_cmd *cmd, t_cline *token_cline, t_op op)
{
	t_rd	*rd;

	rd = rd_new();
	rd->op = op;
	if (op == OP_REIN || op == OP_REIN_IO || op == OP_HDOC)
		rd->from = token_cline;
	else
		rd->to = token_cline;
	if (cmd->token_list && (op == OP_REIN_IO || op == OP_REOUT_IO ||
			op == OP_REOUTAP_IO || op == OP_REOUTFD_IO))
		get_redir_num(&cmd->token_list, rd, op);
	rd_push(&cmd->rd_list, rd);
}

static void	parse_token(t_cmd *cmd, t_cline *token_cline, t_op op)
{
	t_token	*token;

	if (op == OP_NOOP)
	{
		token = token_new(token_cline);
		cmd_push_token(cmd, token);
	}
	else
		parse_redirection(cmd, token_cline, op);
}

static void	parse_pipe(t_pipe *pipe, t_cline *token_cline, t_op op)
{
	t_cmd	*cmd;

	if (op == OP_PIPE || pipe->cmd_list == NULL)
	{
		cmd = cmd_new();
		pipe_push_cmd(pipe, cmd);
		op = OP_NOOP;
	}
	else
	{
		cmd = pipe->cmd_list;
		while (cmd->next)
			cmd = cmd->next;
	}
	parse_token(cmd, token_cline, op);
}

void		create_ast(t_pipe **pipe_list_ptr, t_cline *token_cline, t_op op)
{
	t_pipe	*pipe;

	if (op == OP_CMDEND || *pipe_list_ptr == NULL)
	{
		pipe = pipe_new();
		push_pipe_list(pipe_list_ptr, pipe);
		op = OP_NOOP;
	}
	else
	{
		pipe = *pipe_list_ptr;
		while (pipe->next)
			pipe = pipe->next;
	}
	parse_pipe(pipe, token_cline, op);
}
