/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipe.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/18 19:43:08 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/21 12:55:49 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse.h"

t_pipe	*pipe_new(void)
{
	t_pipe	*pipe;

	if (!(pipe = (t_pipe *)malloc(sizeof(t_pipe))))
		return (NULL);
	pipe->cmd_list = NULL;
	pipe->next = NULL;
	return (pipe);
}

t_cmd	*pipe_push_cmd(t_pipe *pipe, t_cmd *cmd)
{
	t_cmd	*cur;

	if (!pipe->cmd_list)
		pipe->cmd_list = cmd;
	else
	{
		cur = pipe->cmd_list;
		while (cur->next)
			cur = cur->next;
		cur->next = cmd;
	}
	return (cmd);
}

void	pipe_free(t_pipe *pipe)
{
	t_cmd	*tmp;

	if (!pipe)
		return ;
	while (pipe->cmd_list)
	{
		tmp = pipe->cmd_list;
		pipe->cmd_list = pipe->cmd_list->next;
		cmd_free(tmp);
	}
	free(pipe);
}

void	free_pipe_list(t_pipe *pipe_list)
{
	t_pipe	*tmp;

	if (!pipe_list)
		return ;
	while (pipe_list)
	{
		tmp = pipe_list;
		pipe_list = pipe_list->next;
		pipe_free(tmp);
	}
}

t_pipe	*push_pipe_list(t_pipe **pipe_list_ptr, t_pipe *pipe)
{
	t_pipe	*cur;

	if (!*pipe_list_ptr)
		*pipe_list_ptr = pipe;
	else
	{
		cur = *pipe_list_ptr;
		while (cur->next)
			cur = cur->next;
		cur->next = pipe;
	}
	return (pipe);
}
