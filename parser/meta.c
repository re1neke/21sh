/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   meta.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/18 18:14:34 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/18 18:15:32 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse.h"

t_meta	is_meta(wchar_t ch)
{
	if (ft_isspace((char)ch))
		return (META_SPACE);
	else if (ch == '|')
		return (META_PIPE);
	else if (ch == ';')
		return (META_COLON);
	else if (ch == '>')
		return (META_GREAT);
	else if (ch == '<')
		return (META_LESS);
	else if (ch == '&')
		return (META_AMSND);
	else
		return (META_NOMETA);
}
