/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cline_to_str.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/09 17:23:48 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/24 19:48:26 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static int		wch_to_str(char *str, wchar_t wch)
{
	if (wch <= 0x7F || MB_CUR_MAX == 1)
	{
		*str = (char)wch;
		return (1);
	}
	else if ((wch <= 0x7FF && wch > 0x7F) || MB_CUR_MAX == 2)
	{
		*str = (char)(((wch >> 6) & 0x1F) | 0xC0);
		*(str + 1) = (char)((wch & 0x3F) | 0x80);
		return (2);
	}
	else if ((wch <= 0xFFFF && wch > 0x7FF) || MB_CUR_MAX == 3)
	{
		*str = (char)(((wch >> 12) & 0xF) | 0xE0);
		*(str + 1) = (char)(((wch >> 6) & 0x3F) | 0x80);
		*(str + 2) = (char)((wch & 0x3F) | 0x80);
		return (3);
	}
	*str = (char)(((wch >> 18) & 0x7) | 0xF0);
	*(str + 1) = (char)(((wch >> 12) & 0x3F) | 0x80);
	*(str + 2) = (char)(((wch >> 6) & 0x3F) | 0x80);
	*(str + 3) = (char)((wch & 0x3F) | 0x80);
	return (4);
}

static size_t	get_cline_len(t_cch *cch)
{
	if (cch->ch <= 0x7F || MB_CUR_MAX == 1)
		return (1);
	else if ((cch->ch <= 0x7FF && cch->ch > 0x7F) || MB_CUR_MAX == 2)
		return (2);
	else if ((cch->ch <= 0xFFFF && cch->ch > 0x7FF) || MB_CUR_MAX == 3)
		return (3);
	else
		return (4);
}

char			*cline_to_str(t_cline *cline)
{
	char	*str;
	t_cch	*cch;
	size_t	i;

	cch = cline->begin;
	i = 0;
	while (cch)
	{
		i += get_cline_len(cch);
		cch = cch->next;
	}
	if (!(str = ft_strnew(i + 1)))
		return (NULL);
	cch = cline->begin;
	i = 0;
	while (cch)
	{
		i += (size_t)wch_to_str(&str[i], cch->ch);
		cch = cch->next;
	}
	return (str);
}
