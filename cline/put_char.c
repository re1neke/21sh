/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_char.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 17:13:12 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/05 13:34:14 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static void	empty_line(t_cline *cline, t_cch *cch)
{
	cline->begin = cch;
	cline->end = cch;
}

static void	in_line(t_cline *cline, t_cch *cch)
{
	if (cline->cursor->prev)
		link_cch(cline->cursor->prev, cch);
	else
		cline->begin = cch;
	link_cch(cch, cline->cursor);
}

static void	end_line(t_cline *cline, t_cch *cch)
{
	link_cch(cline->end, cch);
	cline->end = cch;
}

t_cch		*push_ch(t_cline *cline, wchar_t ch)
{
	t_cch	*cch;

	cch = new_cch(ch);
	if (!cch)
		return (NULL);
	if (!cline->begin || !cline->end)
		empty_line(cline, cch);
	else if (cline->cursor)
		in_line(cline, cch);
	else
		end_line(cline, cch);
	cline->len++;
	cline->pos++;
	return (cch);
}

void		put_ch(t_cline *cline, wchar_t ch)
{
	int		printed;
	t_cch	*cch;

	cch = push_ch(cline, ch);
	if (!cch)
		return ;
	printed = print_cline(cch);
	cline->cursor = NULL;
	cline->pos = cline->len;
	move_left_for(&cline, printed - 1);
}
