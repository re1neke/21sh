/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cline_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/07 17:50:21 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 14:58:14 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

int		is_space_cline(t_cline *cline)
{
	t_cch	*cch;

	cch = cline->begin;
	while (cch)
	{
		if (!ft_isspace((char)cch->ch))
			return (0);
		cch = cch->next;
	}
	return (1);
}

int		is_num_cline(t_cline *cline)
{
	t_cch	*cch;

	cch = cline->begin;
	while (cch)
	{
		if (!ft_isdigit((char)cch->ch))
			return (0);
		cch = cch->next;
	}
	return (1);
}

t_cline	*split_cline(t_cline *cline, int pos)
{
	t_cline	*new;
	t_cch	*cur;

	if (!pos)
		return (copy_cline(cline, 0));
	if (pos >= cline->len || !(new = new_cline()))
		return (NULL);
	new->len = cline->len - pos;
	new->pos = new->len;
	new->end = cline->end;
	cline->cursor = NULL;
	cline->len = pos;
	cline->pos = cline->len;
	cur = cline->begin;
	while (pos--)
		cur = cur->next;
	new->begin = cur;
	cline->end = cur->prev;
	new->begin->prev = NULL;
	cline->end->next = NULL;
	return (new);
}

int		cline_to_int(t_cline *cline)
{
	int		num;
	char	*num_str;

	if (!(num_str = cline_to_str(cline)))
		return (0);
	num = ft_atoi(num_str);
	free(num_str);
	return (num);
}

int		cline_cmp(t_cline *c1, t_cline *c2)
{
	t_cch	*cch1;
	t_cch	*cch2;

	cch1 = c1->begin;
	cch2 = c2->begin;
	while (cch1 && cch2)
	{
		if (cch1->ch != cch2->ch)
			break ;
		cch1 = cch1->next;
		cch2 = cch2->next;
	}
	return (cch1 == NULL && cch2 == NULL);
}
