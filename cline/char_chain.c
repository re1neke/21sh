/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   char_chain.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 17:11:37 by mvladymy          #+#    #+#             */
/*   Updated: 2019/04/29 19:54:07 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

t_cch	*new_cch(wchar_t ch)
{
	t_cch	*chain;

	chain = (t_cch*)malloc(sizeof(t_cch));
	if (!chain)
		return (NULL);
	chain->ch = ch;
	chain->prev = NULL;
	chain->next = NULL;
	return (chain);
}

void	link_cch(t_cch *left, t_cch *right)
{
	if (!left || !right)
		return ;
	left->next = right;
	right->prev = left;
}

void	del_cch(t_cch *chain)
{
	if (!chain)
		return ;
	if (chain->prev)
		chain->prev->next = chain->next;
	if (chain->next)
		chain->next->prev = chain->prev;
	free(chain);
}
