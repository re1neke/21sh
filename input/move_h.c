/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_h.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 17:47:17 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/06 14:20:55 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static int	get_line_len(t_cch *line_end)
{
	int			line_len;
	extern int	g_prompt_len;

	line_len = 0;
	while ((line_end = line_end->prev) && line_end->ch != '\n')
		line_len++;
	if (!line_end)
		line_len += g_prompt_len;
	return (line_len);
}

void		move_left(t_cline **cline_ptr)
{
	t_cline	*cline;
	int		line_len;

	cline = *cline_ptr;
	if (!cline->begin || (cline->cursor && !cline->cursor->prev))
		return ;
	if ((cline->cursor && cline->cursor->prev->ch == '\n')
			|| (!cline->cursor && cline->end->ch == '\n'))
	{
		line_len = get_line_len(
				(cline->cursor) ? cline->cursor->prev : cline->end);
		ft_dprintf(STDIN_FILENO, tgetstr("up", NULL));
		move_cursor(line_len % g_scr.width);
	}
	else
		move_cursor(-1);
	if (!cline->cursor)
		cline->cursor = cline->end;
	else if (cline->cursor->prev)
		cline->cursor = cline->cursor->prev;
	cline->pos--;
}

void		move_right(t_cline **cline_ptr)
{
	t_cline	*cline;

	cline = *cline_ptr;
	if (!cline->cursor)
		return ;
	if (cline->cursor->ch == '\n')
	{
		ft_dprintf(STDIN_FILENO, tgetstr("do", NULL));
		g_scr.pos = 0;
	}
	else
		move_cursor(1);
	cline->cursor = cline->cursor->next;
	cline->pos++;
}

void		move_left_for(t_cline **cline_ptr, int offset)
{
	while (offset--)
		move_left(cline_ptr);
}

void		move_right_for(t_cline **cline_ptr, int offset)
{
	while (offset--)
		move_right(cline_ptr);
}
