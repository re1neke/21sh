/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backspace.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 19:15:11 by mvladymy          #+#    #+#             */
/*   Updated: 2019/04/29 19:54:02 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

void		backspace(t_cline *cline)
{
	if (cline->cursor == cline->begin)
		return ;
	move_left(&cline);
	del(&cline);
}
