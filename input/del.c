/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   del.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 17:46:59 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/03 20:00:56 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static void	alone_char(t_cline *cline)
{
	del_cch(cline->cursor);
	cline->begin = NULL;
	cline->end = NULL;
	cline->cursor = NULL;
}

static void	first_char(t_cline *cline)
{
	cline->begin = cline->begin->next;
	del_cch(cline->cursor);
	cline->cursor = cline->begin;
}

static void	last_char(t_cline *cline)
{
	del_cch(cline->cursor);
	cline->end = cline->end->prev;
	cline->cursor = NULL;
}

void		del(t_cline **cline_ptr)
{
	int		printed;
	t_cline	*cline;

	cline = *cline_ptr;
	if (!cline->begin || !cline->end || !cline->cursor)
		return ;
	if (cline->cursor == cline->begin && cline->cursor == cline->end)
		alone_char(cline);
	else if (cline->cursor == cline->begin)
		first_char(cline);
	else if (cline->cursor == cline->end)
		last_char(cline);
	else
	{
		cline->cursor = cline->cursor->next;
		del_cch(cline->cursor->prev);
	}
	cline->len--;
	printed = print_cline(cline->cursor);
	ft_dprintf(STDIN_FILENO, tgetstr("cd", NULL));
	cline->cursor = NULL;
	cline->pos = cline->len;
	move_left_for(&cline, printed);
}
