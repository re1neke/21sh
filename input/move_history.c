/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_history.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 20:03:03 by mvladymy          #+#    #+#             */
/*   Updated: 2019/04/30 19:34:35 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

void	next_cline(t_cline **cline_ptr)
{
	t_cline	*cline;

	move_left_for(cline_ptr, (*cline_ptr)->pos);
	ft_dprintf(STDIN_FILENO, tgetstr("cd", NULL));
	next_hist_node(cline_ptr);
	cline = *cline_ptr;
	print_cline(cline->begin);
	cline->pos = cline->len;
	cline->cursor = NULL;
}

void	prev_cline(t_cline **cline_ptr)
{
	t_cline	*cline;

	move_left_for(cline_ptr, (*cline_ptr)->pos);
	ft_dprintf(STDIN_FILENO, tgetstr("cd", NULL));
	prev_hist_node(cline_ptr);
	cline = *cline_ptr;
	print_cline(cline->begin);
	cline->pos = cline->len;
	cline->cursor = NULL;
}
