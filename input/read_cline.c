/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_cline.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/02 18:49:49 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 11:48:51 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static inline void	esc_seq_cline_hdlr(t_cline **cline)
{
	t_esc_hdlr	esc_hdlr;

	esc_hdlr = g_esc_hdlrs[get_esc_seq()];
	if (esc_hdlr)
		esc_hdlr(cline);
}

static void			redraw_cline(t_cline **cline)
{
	int	pos;

	ft_dprintf(STDIN_FILENO, tgetstr("rc", NULL));
	ft_dprintf(STDIN_FILENO, tgetstr("cd", NULL));
	prompt(NO_CAUSE);
	set_term();
	print_cline((*cline)->begin);
	(*cline)->cursor = NULL;
	pos = (*cline)->len - (*cline)->pos;
	(*cline)->pos = (*cline)->len;
	move_left_for(cline, pos);
}

static t_rs			sig_hdlr(t_cline **cline)
{
	extern int	g_sig;

	if (g_sig == SIGINT)
	{
		move_right_for(cline, (*cline)->len - (*cline)->pos);
		clear_cline(*cline);
		reset_hist_cursor();
		return (READ_DROP);
	}
	else if (g_sig == SIGQUIT)
		return (READ_EXIT);
	else if (g_sig == SIGWINCH)
		redraw_cline(cline);
	return (READ_OK);
}

static t_rs			buff_handler(t_cline **cline, wchar_t buff)
{
	if (buff == KEY_EOT && !(*cline)->len)
		return (READ_EXIT);
	else if (buff == KEY_EOT)
		del(cline);
	else if (buff == KEY_ESC)
		esc_seq_cline_hdlr(cline);
	else if (buff == KEY_DEL)
		backspace(*cline);
	else if (buff == KEY_R)
		copy_input_cline(*cline);
	else if (buff == KEY_W)
		paste_input_cline(*cline);
	else if (buff >= ' ')
		put_ch(*cline, buff);
	else if (buff == -1)
		return (sig_hdlr(cline));
	return (READ_OK);
}

t_rs				read_cline(t_cline **cline)
{
	wchar_t buff;
	t_rs	status;

	if (!(*cline = new_cline()))
		return (READ_EXIT);
	status = READ_OK;
	while ((buff = ft_getwchar()) != KEY_EOL)
	{
		status = buff_handler(cline, buff);
		if (status == READ_EXIT)
		{
			free_cline(*cline);
			*cline = NULL;
			break ;
		}
		else if (status == READ_DROP)
			break ;
	}
	return (status);
}
