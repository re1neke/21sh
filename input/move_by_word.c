/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_by_word.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 17:47:11 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/03 19:44:19 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

void	next_word(t_cline **cline_ptr)
{
	bool	is_word;
	t_cline	*cline;

	cline = *cline_ptr;
	is_word = true;
	while (cline->cursor && (ft_isspace((char)cline->cursor->ch) || is_word))
	{
		if (ft_isspace((char)cline->cursor->ch))
			is_word = false;
		move_right(cline_ptr);
	}
}

void	prev_word(t_cline **cline_ptr)
{
	bool	is_space;
	t_cline	*cline;

	cline = *cline_ptr;
	if (!cline->begin || !cline->end)
		return ;
	else if (!cline->cursor)
		move_left(cline_ptr);
	is_space = true;
	while (cline->cursor->prev &&
			(!ft_isspace((char)cline->cursor->prev->ch) || is_space))
	{
		if (!ft_isspace((char)cline->cursor->prev->ch))
			is_space = false;
		move_left(cline_ptr);
	}
}
