/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_v.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/05 08:34:42 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/07 12:56:43 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static bool	len_to_line_begin(t_cch *cursor, int *len)
{
	extern int	g_prompt_len;

	if (!cursor)
	{
		*len = 0;
		return (!cursor);
	}
	*len = 1;
	while ((cursor = cursor->prev) && cursor->ch != '\n')
		(*len)++;
	if (!cursor)
		*len += g_prompt_len;
	return (!cursor);
}

static bool	len_to_line_end(t_cch *cursor, int *len)
{
	if (!cursor)
		return (!cursor);
	else if (cursor->ch == '\n')
	{
		*len = 0;
		return (!cursor);
	}
	*len = 1;
	while ((cursor = cursor->next) && cursor->ch != '\n')
		(*len)++;
	return (!cursor);
}

void		move_down(t_cline **cline_ptr)
{
	t_cline	*cline;
	int		to_begin;
	int		to_end;
	bool	last_line;

	cline = *cline_ptr;
	if (!cline->cursor)
		return ;
	len_to_line_begin(cline->cursor, &to_begin);
	to_begin %= g_scr.width;
	last_line = len_to_line_end(cline->cursor, &to_end);
	if (to_end >= g_scr.width)
		move_right_for(cline_ptr, g_scr.width);
	else if (to_end < g_scr.width
			&& to_end > g_scr.width - g_scr.pos)
		move_right_for(cline_ptr, to_end);
	else if (!last_line)
	{
		move_right_for(cline_ptr, to_end + 1);
		last_line = len_to_line_end(cline->cursor, &to_end);
		if (!to_end)
			return ;
		move_right_for(cline_ptr,
				((to_end > to_begin) ? (to_begin - 1) : to_end));
	}
}

void		move_up(t_cline **cline_ptr)
{
	t_cline	*cline;
	int		to_begin;
	int		prev_line;
	bool	first_line;

	cline = *cline_ptr;
	first_line = len_to_line_begin((cline->cursor) ? cline->cursor : cline->end,
			&to_begin);
	to_begin += (cline->cursor) ? 0 : 1;
	if (to_begin > g_scr.width)
		move_left_for(cline_ptr, g_scr.width);
	else if (first_line && to_begin < g_scr.width)
		return ;
	else
		move_left_for(cline_ptr, to_begin % g_scr.width);
	first_line = len_to_line_begin(cline->cursor, &prev_line);
	if (prev_line % g_scr.width > to_begin)
		move_left_for(cline_ptr, prev_line % g_scr.width - to_begin);
}
