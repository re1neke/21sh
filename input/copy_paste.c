/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copy_paste.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 11:41:19 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 14:35:16 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static t_cline	g_cline_buff;

void			copy_input_cline(t_cline *cline)
{
	clear_cline(&g_cline_buff);
	if (!cline->cursor)
		return ;
	g_cline_buff.begin = cline->cursor;
	g_cline_buff.end = cline->end;
	g_cline_buff.len = cline->len - cline->pos;
	g_cline_buff.pos = g_cline_buff.len;
	cline->len = cline->pos;
	if (!cline->cursor->prev)
	{
		cline->begin = NULL;
		cline->end = NULL;
	}
	else
	{
		cline->end = cline->cursor->prev;
		cline->end->next = NULL;
	}
	cline->cursor = NULL;
	ft_dprintf(STDIN_FILENO, tgetstr("cd", NULL));
}

static void		paste_in_middle(t_cline *cline, t_cline *new_cline)
{
	if (cline->cursor == cline->begin)
		cline->begin = new_cline->begin;
	else
		cline->cursor->prev->next = new_cline->begin;
	new_cline->begin->prev = cline->cursor->prev;
	cline->cursor->prev = new_cline->end;
	new_cline->end->next = cline->cursor;
}

static void		paste_in_end(t_cline *cline, t_cline *new_cline)
{
	cline->end->next = new_cline->begin;
	new_cline->begin->prev = cline->end;
	cline->end = new_cline->end;
}

void			paste_input_cline(t_cline *cline)
{
	t_cline	*new_cline;
	int		move;

	if (!g_cline_buff.len)
		return ;
	if (!(new_cline = copy_cline(&g_cline_buff, g_cline_buff.len)))
		return ;
	move = cline->len - cline->pos;
	if (cline->cursor)
		paste_in_middle(cline, new_cline);
	else if (cline->end)
		paste_in_end(cline, new_cline);
	else
	{
		cline->begin = new_cline->begin;
		cline->end = new_cline->end;
	}
	cline->len += new_cline->len;
	cline->pos = cline->len;
	cline->cursor = NULL;
	print_cline(new_cline->begin);
	move_left_for(&cline, move);
	free(new_cline);
}
