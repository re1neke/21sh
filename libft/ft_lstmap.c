/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 14:20:40 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/14 14:28:06 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static void	del_content(void *content, size_t content_size)
{
	if (content_size && content)
		free(content);
}

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*result;
	t_list	*elem;

	if (!lst || !f)
		return (NULL);
	result = NULL;
	elem = NULL;
	while (lst)
	{
		elem = f(lst);
		if (!elem)
		{
			ft_lstdel(&result, &del_content);
			return (NULL);
		}
		if (!result)
			result = elem;
		else
			ft_lstradd(&result, elem);
		lst = lst->next;
	}
	return (result);
}
