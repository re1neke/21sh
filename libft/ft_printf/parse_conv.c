/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_conv.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 13:11:44 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/17 17:02:15 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

static void	handle_signed(t_conv *conv_atr, va_list *ap)
{
	if (conv_atr->conv_flags & CONV_FLAG_SIZET)
		read_signed_num(va_arg(*ap, ssize_t), conv_atr, 10);
	else if (conv_atr->conv_flags & CONV_FLAG_IMAX)
		read_signed_num((intmax_t)va_arg(*ap, intmax_t), conv_atr, 10);
	else if (conv_atr->conv_flags & CONV_FLAG_LLONG)
		read_signed_num(va_arg(*ap, long long int), conv_atr, 10);
	else if (conv_atr->conv_flags & CONV_FLAG_LONG)
		read_signed_num(va_arg(*ap, long int), conv_atr, 10);
	else if (conv_atr->conv_flags & CONV_FLAG_CHAR)
		read_signed_num((char)va_arg(*ap, int), conv_atr, 10);
	else if (conv_atr->conv_flags & CONV_FLAG_SHORT)
		read_signed_num((short int)va_arg(*ap, int), conv_atr, 10);
	else
		read_signed_num((int)va_arg(*ap, int), conv_atr, 10);
}

static void	handle_unsigned(t_conv *conv_atr, va_list *ap, uint8_t base)
{
	if (conv_atr->conv_flags & CONV_FLAG_SIZET)
		READ_UNSIGNED_NUM(va_arg(*ap, size_t), conv_atr, base);
	else if (conv_atr->conv_flags & CONV_FLAG_IMAX)
		READ_UNSIGNED_NUM((uintmax_t)va_arg(*ap, uintmax_t), conv_atr, base);
	else if (conv_atr->conv_flags & CONV_FLAG_LLONG)
		READ_UNSIGNED_NUM(va_arg(*ap, unsigned long long int), conv_atr, base);
	else if (conv_atr->conv_flags & CONV_FLAG_LONG)
		READ_UNSIGNED_NUM(va_arg(*ap, unsigned long int), conv_atr, base);
	else if (conv_atr->conv_flags & CONV_FLAG_CHAR)
		READ_UNSIGNED_NUM((unsigned char)va_arg(*ap, unsigned int),
				conv_atr, base);
	else if (conv_atr->conv_flags & CONV_FLAG_SHORT)
		READ_UNSIGNED_NUM((unsigned short int)va_arg(*ap, unsigned int),
				conv_atr, base);
	else
		READ_UNSIGNED_NUM((unsigned int)va_arg(*ap, unsigned int),
				conv_atr, base);
}

static void	handle_num_conversion(t_conv *conv_atr, va_list *ap)
{
	if (conv_atr->conv_type == 'd' || conv_atr->conv_type == 'i')
		handle_signed(conv_atr, ap);
	else if (conv_atr->conv_type == 'u')
		handle_unsigned(conv_atr, ap, 10);
	else if (conv_atr->conv_type == 'o')
		handle_unsigned(conv_atr, ap, 8);
	else if (conv_atr->conv_type == 'x' || conv_atr->conv_type == 'X')
		handle_unsigned(conv_atr, ap, 16);
	else if (conv_atr->conv_type == 'b')
		handle_unsigned(conv_atr, ap, 2);
	else if (conv_atr->conv_type == 'D')
		read_signed_num(va_arg(*ap, long int), conv_atr, 10);
	else if (conv_atr->conv_type == 'U')
		READ_UNSIGNED_NUM(va_arg(*ap, unsigned long int), conv_atr, 10);
	else if (conv_atr->conv_type == 'O')
		READ_UNSIGNED_NUM(va_arg(*ap, unsigned long int), conv_atr, 8);
	else if (conv_atr->conv_type == 'p')
		READ_UNSIGNED_NUM((uint64_t)va_arg(*ap, void *), conv_atr, 16);
}

static void	handle_conversion(t_conv *conv_atr, va_list *ap)
{
	if (conv_atr->conv_type == 'S' ||
	(conv_atr->conv_type == 's' && conv_atr->conv_flags & CONV_FLAG_LONG))
		read_unistr(va_arg(*ap, wchar_t *), conv_atr);
	else if (conv_atr->conv_type == 's')
		read_str(va_arg(*ap, char *), conv_atr);
	else if (conv_atr->conv_type == 'C' ||
	(conv_atr->conv_type == 'c' && conv_atr->conv_flags & CONV_FLAG_LONG))
		read_unichar(va_arg(*ap, wchar_t), conv_atr);
	else if (conv_atr->conv_type == 'c')
		read_char((unsigned char)va_arg(*ap, int), conv_atr);
	else
		read_char(conv_atr->conv_type, conv_atr);
}

size_t		parse_conv(char *conv_str, va_list *ap)
{
	t_conv	conv_atr;
	size_t	pos;

	conv_atr.conv_flags = CONV_FLAG_EMPTY;
	conv_atr.conv_width = 0;
	conv_atr.conv_precision = 0;
	pos = parse_atr(ap, conv_str, &conv_atr);
	if (ft_isprint(conv_str[pos]))
	{
		conv_atr.conv_type = conv_str[pos];
		if (conv_atr.conv_type == 'd' || conv_atr.conv_type == 'i' ||
				conv_atr.conv_type == 'U' || conv_atr.conv_type == 'u' ||
				conv_atr.conv_type == 'X' || conv_atr.conv_type == 'x' ||
				conv_atr.conv_type == 'O' || conv_atr.conv_type == 'o' ||
				conv_atr.conv_type == 'b' || conv_atr.conv_type == 'p' ||
				conv_atr.conv_type == 'D')
			handle_num_conversion(&conv_atr, ap);
		else
			handle_conversion(&conv_atr, ap);
		pos++;
	}
	return (pos);
}
