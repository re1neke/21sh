/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 14:38:31 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/05 15:40:28 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t	index;
	uint8_t	u_c;
	uint8_t	*u_s;

	index = 0;
	u_c = (uint8_t)c;
	u_s = (uint8_t *)s;
	while (index < n)
	{
		if (u_s[index] == u_c)
			return (&u_s[index]);
		index++;
	}
	return (NULL);
}
