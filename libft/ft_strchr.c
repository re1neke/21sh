/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 19:59:01 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/04 16:01:42 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	size_t	index;
	size_t	len;
	char	c_c;

	index = 0;
	c_c = (char)c;
	len = ft_strlen(s);
	while (index <= len)
	{
		if (s[index] == c_c)
			return ((char *)&s[index]);
		index++;
	}
	return (NULL);
}
