/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsubst.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/28 15:07:55 by mvladymy          #+#    #+#             */
/*   Updated: 2017/12/13 12:18:58 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsubst(const char *s, char src, char result)
{
	char	*res;
	size_t	idx;

	res = ft_strdup(s);
	if (!res)
		return (NULL);
	idx = 0;
	while (res[idx] != '\0')
	{
		if (res[idx] == src)
			res[idx] = result;
		idx++;
	}
	return (res);
}
