/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/19 16:21:52 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/30 15:14:53 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <locale.h>
#include "input.h"
#include "parse.h"
#include "builtins.h"
#include "exec.h"

/*
** #define LEAK_CHECK
*/

#if defined(LEAK_CHECK) && defined(__APPLE__)
# define CHECK_LEAKS() (system("leaks 21sh"))
#else
# define CHECK_LEAKS()
#endif

int		main(void)
{
	extern char	**environ;

	g_env = copy_env(environ);
	start_env(&g_env);
	setlocale(LC_ALL, "");
	shell_loop(cline_handler);
	free_env(g_env);
	CHECK_LEAKS();
	return (0);
}
